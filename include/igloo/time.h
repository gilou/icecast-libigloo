/* Copyright (C) 2021-2022  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__TIME_H_
#define _LIBIGLOO__TIME_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <time.h>
#include <igloo/types.h>

typedef struct {
    /* internal details. do not access directly! */
    uint32_t flags;
    uint32_t ssec;
    uint64_t fsec;
} igloo_ctime_t;

typedef uint32_t igloo_clock_t;

#define igloo_CLOCK_REALTIME        ((igloo_clock_t)1) /* wall clock. Might jump by any amount in any direction. */
#define igloo_CLOCK_MONOTONIC       ((igloo_clock_t)2) /* monotonic clock. Might not jump backwards. */

/* Constant for the most monotonic clock that is available.
 * This should be used when sleeping or calculating time differences.
 */
#ifdef __MINGW32__ /* There is no igloo_CLOCK_MONOTONIC on win* */
#define igloo_CLOCK_MOST_MONOTONIC  igloo_CLOCK_REALTIME
#else
#define igloo_CLOCK_MOST_MONOTONIC  igloo_CLOCK_MONOTONIC
#endif

/* Create a igloo_ctime_t representing a NULL-value.
 *
 * To create a zero value use igloo_ctime_from_time_t() or igloo_ctime_from_interval() with a value of 0.
 */
igloo_error_t   igloo_ctime_from_null(igloo_ctime_t *dst);

/* Create a absolute igloo_ctime_t from a time_t. */
igloo_error_t   igloo_ctime_from_time_t(igloo_ctime_t *dst, time_t src, igloo_clock_t clock);
/* Create a interval igloo_ctime_t from a number of seconds + nano seconds. */
igloo_error_t   igloo_ctime_from_interval(igloo_ctime_t *dst, uint64_t sec, uint32_t nsec, igloo_clock_t clock);
/* Create a absolute igloo_ctime_t from the current time. */
igloo_error_t   igloo_ctime_from_now(igloo_ctime_t *dst, igloo_clock_t clock);
/* Convert a igloo_ctime_t to a time_t. */
igloo_error_t   igloo_ctime_to_time_t(time_t *dst, igloo_ctime_t src);
/* Sleep for the duration of a interval igloo_ctime_t. */
igloo_error_t   igloo_ctime_sleep(igloo_ctime_t t);

/* Check if a igloo_ctime_t is a NULL-value. */
bool            igloo_ctime_is_null(igloo_ctime_t t);
/* Check if a igloo_ctime_t is valid. */
bool            igloo_ctime_is_valid(igloo_ctime_t t);
/* Check if a igloo_ctime_t represents an interval. */
bool            igloo_ctime_is_interval(igloo_ctime_t t);
/* Ćheck if a igloo_ctime_t represents an absolute time. */
bool            igloo_ctime_is_absolute(igloo_ctime_t t);
/* Check if a igloo_ctime_t is negative.
 *
 * This can only return true for intervals as absolute times
 * are always considered positive.
 */
bool            igloo_ctime_is_negative(igloo_ctime_t t);

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__TIME_H_ */
