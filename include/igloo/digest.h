#ifndef __LIBIGLOO_DIGEST_H__
#define __LIBIGLOO_DIGEST_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <igloo/ro.h>

igloo_RO_FORWARD_TYPE(igloo_digest_t);
igloo_RO_FORWARD_TYPE(igloo_hmac_t);

/* creates a new digest object for the given algo in the given group. Most often an instance object is passed as group */
igloo_error_t igloo_digest_new(igloo_digest_t **self, igloo_ro_t group, const char *algo);
/* copies an digest object. This is not supported for all algos */
igloo_error_t igloo_digest_copy(igloo_digest_t **new, igloo_ro_t group, igloo_digest_t *old);
/* write data to the digest object. Returns the number of bytes actually consumed */
ssize_t igloo_digest_write(igloo_digest_t *digest, const void *data, size_t len);
/* finalises the digest and reads the result as binary. len must be long enough to read all of the hash. Returns the actual number of bytes returned */
ssize_t igloo_digest_read(igloo_digest_t *digest, void *buf, size_t len);

igloo_error_t igloo_hmac_new(igloo_hmac_t **self, igloo_ro_t group, const char *algo, const void *key, size_t keylen);
igloo_error_t igloo_hmac_copy(igloo_hmac_t **new, igloo_ro_t group, igloo_hmac_t *old); /* the same limitations apply as for igloo_digest_copy() */
ssize_t igloo_hmac_write(igloo_hmac_t *hmac, const void *data, size_t len);
ssize_t igloo_hmac_read(igloo_hmac_t *hmac, void *buf, size_t len);

#ifdef __cplusplus
}
#endif

#endif
