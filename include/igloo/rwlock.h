/* Copyright (C) 2020       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__RWLOCK_H_
#define _LIBIGLOO__RWLOCK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h> /* for size_t */
#include <pthread.h>

typedef struct {
    pthread_rwlock_t lock;
    size_t wlockc;
    pthread_t writer;
} igloo_rwlock_t;

void igloo_rwlock_init(igloo_rwlock_t *lock);
void igloo_rwlock_destroy(igloo_rwlock_t *lock);
void igloo_rwlock_rlock(igloo_rwlock_t *lock);
void igloo_rwlock_wlock(igloo_rwlock_t *lock);
void igloo_rwlock_unlock(igloo_rwlock_t *lock);

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__RWLOCK_H_ */
