#!/bin/bash 

set -xeu -o pipefail

LIBIGLOO_VERSION=${1:?Version to build, e.g. 0.9.3}; shift
ARCHIVE_VERSION=${1:?CI or RELEASE? _VERSION_ARCHIVE_ for release, whatever for CI}; shift
CI_VERSION=${1:?Missing CI Version - Use 0.9.2+bla, used for packages doc and changelog}; shift
DATE=$(date --date="${1:?ISO DATE or now}" --iso-8601=seconds); shift
AUTHOR=${1:?Mail Address}; shift
TEXT=${1:?Release Text}; shift
LIBIGLOO_PROJECT=${1:?libigloo OSC Project Name}; shift
W32_LIBIGLOO_PROJECT=${1:?libigloo W32 OSC Project Name}; shift

OSC_BASE_DIR="${OSC_TMP:-osc_tmp}"

# upon release we modify the templates - in ci we modiy temporary files
if [ "$ARCHIVE_VERSION" = "_VERSION_ARCHIVE_" ]; then
  OSC_BASE_DIR=ci/osc
fi

mkdir -p $OSC_BASE_DIR
pushd $OSC_BASE_DIR

sed -i "1s#^#libigloo ($CI_VERSION-1) UNRELEASED; urgency=medium\n\n  * $TEXT\n\n --  $AUTHOR  $(LC_TIME=C date --date="$DATE" +"%a, %d %b %Y %H:%M:%S %z")\n\n#"  "$LIBIGLOO_PROJECT/debian/changelog"

for i in "$LIBIGLOO_PROJECT/$LIBIGLOO_PROJECT.spec" "$W32_LIBIGLOO_PROJECT/$W32_LIBIGLOO_PROJECT.spec"; do
  sed -i "s/_VERSION_ARCHIVE_/$ARCHIVE_VERSION/; s/^Version:\(\s*\)[^\s]*$/Version:\1$CI_VERSION/; s#^%changelog.*\$#\0\n* $(LC_TIME=C date --date="$DATE" +"%a %b %d %Y") $AUTHOR - $CI_VERSION-1\n- $TEXT\n\n#" "$i";
done

popd

# Careful, this does NOT work for -xxx labels, as it's not used for libigloo for now
IFS='.' read -r LIBIGLOO_VERSION_MAJOR LIBIGLOO_VERSION_MINOR LIBIGLOO_VERSION_PATCH <<< "$LIBIGLOO_VERSION"
sed -i "1s#m4_define(libigloo_major, [0-9]\+)#m4_define(libigloo_major, $LIBIGLOO_VERSION_MAJOR)#" configure.ac
sed -i "2s#m4_define(libigloo_minor, [0-9]\+)#m4_define(libigloo_minor, $LIBIGLOO_VERSION_MINOR)#" configure.ac
sed -i "3s#m4_define(libigloo_patch, [0-9]\+)#m4_define(libigloo_patch, $LIBIGLOO_VERSION_PATCH)#" configure.ac

sed -i "s/^\(export LIBIGLOO_VERSION=\).*$/\1$LIBIGLOO_VERSION/" ci/osc/*-config.sh

# we only do this for release builds
if [ "$ARCHIVE_VERSION" = "_VERSION_ARCHIVE_" ]; then
  sed -i "s/^\(export RELEASE_AUTHOR=\"\).*\(\"\)$/\1$AUTHOR\2/; s/\(export RELEASE_DATETIME=\).*$/\1$DATE/" ci/osc/release-config.sh
fi

if [ "$ARCHIVE_VERSION" != "_VERSION_ARCHIVE_" ]; then
  if ! git diff --quiet; then
    echo "git detected differences after ci driven create changelog run, this should not happen - please check";
    git status
    git --no-pager diff
    exit 1;
  else
    echo "no repo diffs detected, this is good as CI should not change the repo(only temp files)!"
  fi
else
  echo "applied changes to versions, please verify and commit them for a new release"
  git status
  git --no-pager diff
fi
