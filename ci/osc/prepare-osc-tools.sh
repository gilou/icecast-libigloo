#!/bin/sh -xe
cat /etc/os*

# We are alone. Let's break stuff.
pip install osc --break-system-packages

OSC_RC_FILE=${OSC_RC_FILE:-$HOME/.config/osc/oscrc}
echo "Using $OSC_RC_FILE as osc config (override with \$OSC_RC_FILE)"

# if $OSC_RC is set, and the file exists, copy it as osc's config

if [ -n "$OSC_RC" ] && [ -f "$OSC_RC" ]; then
  echo "Found config at $OSC_RC, copying to $OSC_RC_FILE"
  install -m 600 "$OSC_RC" -D "$OSC_RC_FILE"
else
  echo "\$OSC_RC not set or file does not exist"
  exit 1
fi
