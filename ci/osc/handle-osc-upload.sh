#!/bin/bash -xe


SCRIPT_DIR=`dirname $0`
SCRIPT_DIR=`realpath $SCRIPT_DIR`
CONFIG=${1:-nightly}
OSC_RC_FILE=${OSC_RC_FILE:-$HOME/.config/osc/oscrc}

. $SCRIPT_DIR/common-config.sh
. $SCRIPT_DIR/$CONFIG-config.sh

: "${CI_PIPELINE_URL:?Variable CI_PIPELINE_URL not set or empty}"

pwd

ls -la

export OSC_CMD="${OSC_CMD:-$HOME/.local/bin/osc} --config=$OSC_RC_FILE"
echo "Using ${OSC_CMD} for \$OSC_CMD, as $(id)"

export SOURCE=`pwd`

for OBS_BASE in $OBS_BASES; do
  export OSC_TMP=osc_tmp-$OBS_BASE

  rm -rf $OSC_TMP
  mkdir -p $OSC_TMP
  cd $OSC_TMP
  $OSC_CMD init $OBS_BASE

  # checkout into a dkirectory named like the project - avoiding having it in a subdir called OBS_BASE
  for i in "$LIBIGLOO_PROJECT" "$W32_LIBIGLOO_PROJECT"; do
    $OSC_CMD checkout -o "$i" $OBS_BASE "$i" || $OSC_CMD mkpac "$i"
    rm -vrf "$i"/*
  done

  # no comment needed
  for i in "$LIBIGLOO_PROJECT" "$W32_LIBIGLOO_PROJECT"; do
    cp $SOURCE/libigloo-$LIBIGLOO_VERSION.tar.gz "$i"/libigloo_$LIBIGLOO_CI_VERSION.orig.tar.gz 
  done

  # we copy the spec for these projects - for the libigloo project the spec is globeed
  for i in "$W32_LIBIGLOO_PROJECT"; do
    cp -a $SCRIPT_DIR/$i/$i.spec $i/
  done

  # this is more complex because we have more files.
  cp -a $SCRIPT_DIR/$LIBIGLOO_PROJECT/libigloo* $LIBIGLOO_PROJECT/
  cp -a $SCRIPT_DIR/$LIBIGLOO_PROJECT/debian $LIBIGLOO_PROJECT/

  if [ "$DISABLE_CHANGELOG" == "0" ]; then
    pushd $SOURCE
      $SCRIPT_DIR/../create-changelog-and-set-versions.sh "$LIBIGLOO_VERSION" "$LIBIGLOO_VERSION" "$LIBIGLOO_CI_VERSION" "$RELEASE_DATETIME" "$RELEASE_AUTHOR" "CI Build - $CI_PIPELINE_URL" "$LIBIGLOO_PROJECT" "$W32_LIBIGLOO_PROJECT"
    popd
  else
    for i in "$LIBIGLOO_PROJECT/$LIBIGLOO_PROJECT.spec" "$W32_LIBIGLOO_PROJECT/$W32_LIBIGLOO_PROJECT.spec"; do
      sed -i "s/_VERSION_ARCHIVE_/$LIBIGLOO_VERSION/;" "$i";
    done
  fi

  tar -C $LIBIGLOO_PROJECT -cvzf $LIBIGLOO_PROJECT/libigloo_$LIBIGLOO_CI_VERSION-1.debian.tar.gz debian/

  # remove debian/ so it does not end up in the archive
  rm -rf $LIBIGLOO_PROJECT/debian

  # we fix the dsc to have the correct hashsums so dpkg-buildpackage and co do not complain
  pushd $LIBIGLOO_PROJECT
  $SCRIPT_DIR/../fix-dsc.sh
  popd

# we use addremove to detect changes and commit them to the server
  for i in "$LIBIGLOO_PROJECT" "$W32_LIBIGLOO_PROJECT"; do
    pushd $i

    $OSC_CMD addremove
    $OSC_CMD diff
    $OSC_CMD commit -m "Commit via $CI_PIPELINE_URL - Tag: ${CI_COMMIT_TAG:-N/A} - Branch: ${GIT_BRANCH:-N/A} - Commit: $GIT_COMMIT - Release Author: ${RELEASE_AUTHOR} - Commit Author: ${CI_COMMIT_AUTHOR}"

    popd
  done

  cd ..
done

# we cleanup because the OSC_RC should not remain on disk
if [ "$NOCLEANUP" != "1" ]; then
  for file in "$OSC_RC" "$OSC_RC_FILE" .osc_cookiejar; do
    [ -n "$file" ] && [ -f "$file" ] && shred -vzf "$file" 
    echo > "$file"
  done
  rm -rf "osc_tmp*"
fi
