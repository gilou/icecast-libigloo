#
# spec file for package libigloo (copied from https://build.opensuse.org/package/view_file/openSUSE:Factory/libvorbis/libvorbis.spec?expand=1)
#
# Copyright (c) 2020 SUSE LLC, 2022 Stephan Jauernick
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

%define version_archive _VERSION_ARCHIVE_

Name:           libigloo
Version:        0.9.3
Release:        0
Summary:        libigloo is a generic C framework. It is developed and used by the Icecast project.
License:        LGPL-2.0-only
Group:          System/Libraries
URL:            https://icecast.org
Source:        libigloo_%{version}.orig.tar.gz
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  automake
BuildRequires:  xz
BuildRequires:  rhash-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
libigloo is a generic C framework. It is developed and used by the Icecast project.

%package -n libigloo0
Summary:        libigloo is a generic C framework. It is developed and used by the Icecast project.
Group:          System/Libraries
Provides:       %{name} = %{version}
Obsoletes:      %{name} < %{version}


%description -n libigloo0
libigloo is a generic C framework. It is developed and used by the Icecast project.

%package devel
Summary:        Include Files and Libraries mandatory for libigloo Development
Group:          Development/Libraries/C and C++
Requires:       glibc-devel
Requires:       libigloo0 = %{version}

%description devel
This package contains all necessary include files and libraries needed
to compile and develop applications that use libigloo.

%prep
%setup -q -n libigloo-%{version_archive}

%build

autoreconf -fiv
export LDFLAGS="$LDFLAGS -ffat-lto-objects"
export CFLAGS="$CFLAGS -ffat-lto-objects"
%configure
make %{?_smp_mflags}

%install
make DESTDIR=%{buildroot} install

%check
make %{?_smp_mflags} check

%post -n libigloo0 -p /sbin/ldconfig

%postun -n libigloo0 -p /sbin/ldconfig

%files -n libigloo0
%{_libdir}/libigloo.so.*

%files devel
%defattr(-,root,root)
%doc README NEWS
%license COPYING
%{_includedir}/igloo
%{_libdir}/lib*.*a
%{_libdir}/lib*.*so
%{_libdir}/pkgconfig/*.pc

%changelog
* Fri Jan 24 2025 Philipp Schafft <phschafft@de.loewenfelsen.net> - 0.9.3-1
- New upstream version 0.9.3


