#
# spec file for package libigloo (copied from https://build.opensuse.org/package/view_file/openSUSE:Factory/libvorbis/libvorbis.spec?expand=1)
#
# Copyright (c) 2020 SUSE LLC, 2022 Stephan Jauernick
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

%define version_archive _VERSION_ARCHIVE_ 

Name:           mingw32-libigloo
Version:        0.9.3
Release:        0
Summary:        MinGW Windows port of libigloo. Which is a generic C framework. It is developed and used by the Icecast project.
License:        LGPL-2.0
Group:          System/Libraries
URL:            https://icecast.org
Source:         libigloo_%{version}.orig.tar.gz
BuildRequires:  libtool
BuildRequires:  automake
BuildRequires:  xz
BuildRequires:  mingw32-cross-binutils
BuildRequires:  mingw32-cross-gcc
BuildRequires:  mingw32-cross-pkg-config
BuildRequires:  mingw32-filesystem >= 35
BuildRequires:  mingw32-librhash-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%_mingw32_package_header_debug
BuildArch:      noarch

%description
MinGW Windows port of libigloo. Which is a generic C framework. It is developed and used by the Icecast project.

%_mingw32_debug_package

%package -n mingw32-libigloo0
Summary:        MinGW Windows port of libigloo. Which is a generic C framework. It is developed and used by the Icecast project.
Group:          System/Libraries
Provides:       %{name} = %{version}
Obsoletes:      %{name} < 0.9.1


%description -n mingw32-libigloo0
MinGW Windows port of libigloo. Which is a generic C framework. It is developed and used by the Icecast project.

%package devel
Summary:        Include Files and Libraries mandatory for libigloo Development
Group:          Development/Libraries/C and C++
Requires:       glibc-devel
Requires:       %{name} = %{version}

%description devel
This package contains all necessary include files and libraries needed
to compile and develop applications that use libigloo.


%prep
%setup -q -n libigloo-%{version_archive} 

%build
autoreconf -fiv
  
echo "lt_cv_deplibs_check_method='pass_all'" >>%{_mingw32_cache}
PATH="%{_mingw32_bindir}:$PATH"; export PATH; \
MINGW32_LDFLAGS="%{_mingw32_ldflags}"; export MINGW32_LDFLAGS; \
MINGW32_CFLAGS="%{_mingw32_cflags}"; export MINGW32_CFLAGS; \
AM_LDFLAGS="-no-undefined"; export AM_LDFLAGS; \
%{_mingw32_configure} --enable-shared --disable-static

%{_mingw32_make} %{?_smp_mflags}

%install
make DESTDIR=%{buildroot} install
# docs are built in a separate spec file
#rm -rf %{buildroot}%{_datadir}/doc/*
# remove unneeded files
find %{buildroot} -type f -name "*.la" -delete -print

%check
echo disabled1
echo disabled2

%post -n mingw32-libigloo0 -p /sbin/ldconfig

%postun -n mingw32-libigloo0 -p /sbin/ldconfig

%files -n mingw32-libigloo0
%{_mingw32_bindir}/libigloo*.dll

%files devel
%defattr(-,root,root)
%doc README NEWS
%license COPYING
%{_mingw32_includedir}/igloo
%{_mingw32_libdir}/lib*
%{_mingw32_libdir}/pkgconfig/*.pc

%changelog
* Fri Jan 24 2025 Philipp Schafft <phschafft@de.loewenfelsen.net> - 0.9.3-1
- New upstream version 0.9.3


