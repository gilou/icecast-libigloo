# Mark the git dir safe, as it may have wider permissions
git config --global --add safe.directory "$PWD"

GIT_BRANCH=${CI_COMMIT_BRANCH}
GIT_COMMIT=`git rev-parse HEAD`

OBS_BASE_PROJECT=${OBS_BASE_PROJECT:-multimedia:xiph}
OBS_BASE_NIGHTLY_MASTER="${OBS_BASE_PROJECT}:nightly-master"
OBS_BASE_NIGHTLY_DEVEL="${OBS_BASE_PROJECT}:nightly-devel"
OBS_BASE_BETA="${OBS_BASE_PROJECT}:beta"
OBS_BASE_RELEASE="${OBS_BASE_PROJECT}"

export LIBIGLOO_PROJECT=libigloo
export W32_LIBIGLOO_PROJECT=mingw32-libigloo
