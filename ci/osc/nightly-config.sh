if [ "z$OBS_BASES" = "z" ]; then
  if [ "$GIT_BRANCH" = "master" ]; then
    export OBS_BASES=$OBS_BASE_NIGHTLY_MASTER
  elif [ "$GIT_BRANCH" = "devel" ]; then
    export OBS_BASES=$OBS_BASE_NIGHTLY_DEVEL
  else
    echo "branch '$GIT_BRANCH' is not master or devel, please export OBS_BASE accordingly";
    exit 1
  fi
fi

export LIBIGLOO_VERSION=0.9.3
export LIBIGLOO_CI_VERSION=$LIBIGLOO_VERSION+`date +%Y%m%d%H%M%S`+$GIT_COMMIT
export DISABLE_CHANGELOG=0
export RELEASE_AUTHOR=${CI_COMMIT_AUTHOR:?Please set CI_COMMIT_AUTHOR}
export RELEASE_DATETIME=now
