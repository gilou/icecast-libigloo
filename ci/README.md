# Libigloo CI for automated OBS Uploads

This directory contains scripting triggered by .gitlab-ci.yml to automatically upload releases and nightlies to build.opensuse.org.
The Pipeline triggers are set so that releases are pushed only on tags and nightlies on master/devel commits into respective OBS projects.
These are defined in the ci/osc/ for releases and nightlies in their respective config files.
The OBS projects uses `$OBS_PROJECT_BASE` for releases (that one can override), and adds suffixes to it to get the subprojects: :nightly-master, :nightly-devel, :beta.

It is possible to restrict CI stages by setting `FAST_TRACK` to true (builds, tests, uploads only on `bookworm/x86_64`) to get a faster pipeline for testing.

# Local Test

Testing will require having osc installed locally or osc-wrapper.py available from source, as well as a working config for OSC in `~/.config/osc/oscrc` (or through `$OSC_RC_FILE`)

`NOCLEANUP=1 CI_COMMIT_BRANCH=devel OBS_BASES=home:yourobs:xiph:test CI_COMMIT_AUTHOR=info@stephan-jauernick.de CI_PIPELINE_URL=localtest ./ci/osc/handle-osc-upload.sh`

# How to release

To make a new release please call the magic version changer as following from the repo root:

```
LIBIGLOO_VERSION=0.9.3
ci/create-changelog-and-set-versions.sh "$LIBIGLOO_VERSION" "_VERSION_ARCHIVE_" "$LIBIGLOO_VERSION" "now" "Stephan Jauernick <info@stephan-jauernick.de>" "Preparing for libigloo $LIBIGLOO_VERSION libigloo mingw32-libigloo
```

Please adapt the `LIBIGLOO_VERSION`, the Author, the Date(now; please enter a valid ISO8601 Date if needed) and the Message as needed.

This script/mechanism will update all the version references and then show you a git status/diff. Please check all changes and commit them as needed.

After tagging and uploading the release will be picked up by gitlabs CI and a release will be pushed to OBS.

# Nightlies

A nightly will be build on each change to master/devel - these will be marked with a git version + build date in the version and a "correct" changelog entry where relevant.

# Required Repo Configuration in Gitlab and OBS

- master and devel need to be set to protected with Maintainer only push/merge permissions
- all tags need to be set to protected with Maintainer only permissions
- There needs to be a "File" Variable configured as follows:

Name: `OSC_RC`
Protected only: yes
Content:
```
[general]
apiurl = https://api.opensuse.org

[https://api.opensuse.org]
user = OBS USER
pass = OBS PASSWORD
```

The referenced user needs to have Maintainer access to the OBS projects referenced in the build configuration files. Since this will be stored on the gitlab instance running the CI, and transmitted to the gitlab runners, it is advised to use a specific user for CI.

For now we use `$OBS_BASES` as the default project to upload stuff, somehow mimicking what we could have on multimedia:xiph. It can be home:you:nightlies or what not. It can be empty as the script will handle creating the repositories, but if you want to build for mingw32, you'll need to inspect the "meta" from https://build.opensuse.org/projects/home:stephan48:branches:multimedia:xiph:beta-devel/meta to get that right, and include mingw32-rhash by using e.g. `osc aggregatepac home:stephan48:branches:multimedia:xiph:beta-devel mingw32-rhash home:you:project`

# How to improve

Rework OBS spec and debian files to modern standards. Try to refactor and generize build/release tooling between libigloo and icecast-server (and maybe others)
