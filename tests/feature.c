/* Copyright (C) 2018-2021  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include <igloo/feature.h>
#include <igloo/tap.h>
#include <igloo/error.h>

igloo_FEATURE_PUBLIC(public_feature);
igloo_FEATURE_PRIVATE(private_feature);

static void test_equal(void)
{
    igloo_tap_test("igloo_feature_equal(public_feature, public_feature) == true", igloo_feature_equal(public_feature, public_feature) == true);
    igloo_tap_test("igloo_feature_equal(private_feature, private_feature) == true", igloo_feature_equal(private_feature, private_feature) == true);
    igloo_tap_test("igloo_feature_equal(public_feature, private_feature) == false", igloo_feature_equal(public_feature, private_feature) == false);
    igloo_tap_test("igloo_feature_equal(private_feature, public_feature) == false", igloo_feature_equal(private_feature, public_feature) == false);
    igloo_tap_test("igloo_feature_equal(public_feature, NULL) == false", igloo_feature_equal(public_feature, NULL) == false);
    igloo_tap_test("igloo_feature_equal(private_feature, NULL) == false", igloo_feature_equal(private_feature, NULL) == false);
}

static void test_get_name(void)
{
    const char *name;

    name = NULL;
    igloo_tap_test_success("igloo_feature_get_name(public_feature, &name)", igloo_feature_get_name(public_feature, &name));
    igloo_tap_test("name != NULL", name != NULL);
    igloo_tap_test("name matches", strcmp(name, "public_feature") == 0);

    name = NULL;
    igloo_tap_test_success("igloo_feature_get_name(private_feature, &name)", igloo_feature_get_name(private_feature, &name));
    igloo_tap_test("name != NULL", name != NULL);
    igloo_tap_test("name matches", strcmp(name, "private_feature (private)") == 0);
}

int main (void) {
    igloo_tap_init();
    igloo_tap_exit_on(igloo_TAP_EXIT_ON_FIN, NULL);
    igloo_tap_group_run("equal", test_equal);
    igloo_tap_group_run("get_name", test_get_name);
    igloo_tap_fin();

    return EXIT_FAILURE; // return failure as we should never reach this point!
}
