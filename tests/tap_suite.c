/* Copyright (C) 2018-2021  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>

#include <igloo/tap.h>
#include <igloo/error.h>

static void cleanup(void) {
    igloo_tap_diagnostic("Cleanup ran.");
}

int main (void) {
    igloo_tap_stats_t stats;

    igloo_tap_init();
    igloo_tap_exit_on(igloo_TAP_EXIT_ON_FIN, cleanup);
    igloo_tap_test("suite working", true);
    igloo_tap_diagnostic("diagnostic message");
    igloo_tap_test_success("get stats", igloo_tap_get_stats(&stats));
    igloo_tap_test("stats count", stats.count == 1);
    igloo_tap_test("stats passed", stats.passed == 1);
    igloo_tap_test("stats failed", stats.failed == 0);
    igloo_tap_fin();

    return EXIT_FAILURE; // return failure as we should never reach this point!
}
